package recfun

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    if (c == 0 || c == r) 1 else pascal(c - 1, r - 1) + pascal(c, r - 1)
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    def calculateBalance(counter: Int, chars: List[Char]): Integer = {
      if (chars.isEmpty || counter < 0) counter
      else {
        val head: Char = chars.head
        val tail: List[Char] = chars.tail
        if (head == '(') calculateBalance(counter + 1, tail)
        else if (head == ')') calculateBalance(counter - 1, tail)
        else calculateBalance(counter, tail)
      }
    }
    calculateBalance(0, chars) == 0
  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {
    def calculateWays(ways: Int, money: Int, coins: List[Int]): Int = {
      if (money < 0) ways
      else if (coins.isEmpty) {
        if (money == 0) ways + 1
        else ways
      }
      else calculateWays(ways, money, coins.tail) + calculateWays(ways, money - coins.head, coins)
    }
    calculateWays(0, money, coins)
  }
}
