package patmat

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import patmat.Huffman._

@RunWith(classOf[JUnitRunner])
class HuffmanSuite extends FunSuite {

  trait TestTrees {
    val t1 = Fork(Leaf('a', 2), Leaf('b', 3), List('a', 'b'), 5)
    val t2 = Fork(Fork(Leaf('a', 2), Leaf('b', 3), List('a', 'b'), 5), Leaf('d', 4), List('a', 'b', 'd'), 9)
    val t3 = Fork(Fork(Leaf('c', 2), Leaf('d', 3), List('c', 'd'), 5), Leaf('e', 4), List('c', 'd', 'e'), 9)
  }

  test("weight of a larger tree") {
    new TestTrees {
      assert(weight(t1) === 5)
    }
  }

  test("weight of a fork t1 t2") {
    new TestTrees {
      assert(weight(Fork(t1, t2, List('a', 'b', 'd'), 14)) === 14)
    }
  }

  test("weight of a leaf") {
    new TestTrees {
      assert(weight(Leaf('a', 4)) === 4)
    }
  }

  test("chars of a larger tree") {
    new TestTrees {
      assert(chars(t2) === List('a', 'b', 'd'))
    }
  }

  test("chars of a  a fork t1 t2") {
    new TestTrees {
      assert(chars(Fork(t1, t2, List('a', 'b', 'd'), 14)) === List('a', 'b', 'd'))
    }
  }

  test("chars of a leaf") {
    new TestTrees {
      assert(chars(Leaf('a', 4)) === List('a'))
    }
  }

  test("string2chars(hello, world)") {
    assert(string2Chars("hello, world") === List('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd'))
  }

  test("makeOrderedLeafList for some frequency table") {
    assert(makeOrderedLeafList(List(('t', 2), ('e', 1), ('x', 3))) === List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 3)))
  }

  test("makeOrderedLeafList for own frequency table") {
    assert(makeOrderedLeafList(List(('a', 3), ('b', 2), ('c', 4), ('d', 1))) === List(Leaf('d', 1), Leaf('b', 2), Leaf('a', 3), Leaf('c', 4)))
  }

  test("times") {
    assert(times(List('a', 'b', 'c', 'd', 'c', 'c', 'c', 'b', 'a', 'a')).sortWith(_._1 < _._1) === List(('a', 3), ('b', 2), ('c', 4), ('d', 1)))
  }

  test("combine of some leaf list") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    assert(combine(leaflist) === List(Fork(Leaf('e', 1), Leaf('t', 2), List('e', 't'), 3), Leaf('x', 4)))
  }

  test("combine of some leaf larger list") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('m', 2), Leaf('x', 4))
    assert(combine(leaflist) === List(Leaf('m', 2), Fork(Leaf('e', 1), Leaf('t', 2), List('e', 't'), 3), Leaf('x', 4)))
  }

  test("combine of some leaf another larger list") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 1), Leaf('m', 1), Leaf('x', 1))
    assert(combine(leaflist) === List(Leaf('m', 1), Leaf('x', 1), Fork(Leaf('e', 1), Leaf('t', 1), List('e', 't'), 2)))
  }

  test("combine of some fork list") {
    new TestTrees {
      val forklist = List(t1, t3)
      assert(combine(forklist) === List(Fork(t1, t3, List('a', 'b', 'c', 'd', 'e'), 14)))
    }
  }

  test("decode and encode a very short text should be identity") {
    new TestTrees {
      assert(decode(t1, encode(t1)("ab".toList)) === "ab".toList)
    }
  }

  test("decode and encode a text should be identity") {
    new TestTrees {
      assert(decode(t2, encode(t2)("abababdddddbbaabbabdbabdbabdbbbabbbdbababbbaadbdbbdab".toList)) === "abababdddddbbaabbabdbabdbabdbbbabbbdbababbbaadbdbbdab".toList)
    }
  }

  test("decode and quick encode a text should be identity") {
    new TestTrees {
      assert(decode(t2, quickEncode(t2)("abababdddddbbaabbabdbabdbabdbbbabbbdbababbbaadbdbbdab".toList)) === "abababdddddbbaabbabdbabdbabdbbbabbbdbababbbaadbdbbdab".toList)
    }
  }

  test("decoded secret") {
    assert(decodedSecret === List('h', 'u', 'f', 'f', 'm', 'a', 'n', 'e', 's', 't', 'c', 'o', 'o', 'l'))
  }
}
