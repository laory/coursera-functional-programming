package funsets

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
 * This class is a test suite for the methods in object FunSets. To run
 * the test suite, you can either:
 * - run the "test" command in the SBT console
 * - right-click the file in eclipse and chose "Run As" - "JUnit Test"
 */
@RunWith(classOf[JUnitRunner])
class FunSetSuite extends FunSuite {


  /**
   * Link to the scaladoc - very clear and detailed tutorial of FunSuite
   *
   * http://doc.scalatest.org/1.9.1/index.html#org.scalatest.FunSuite
   *
   * Operators
   * - test
   * - ignore
   * - pending
   */

  /**
   * Tests are written using the "test" operator and the "assert" method.
   */
  test("string take") {
    val message = "hello, world"
    assert(message.take(5) == "hello")
  }

  /**
   * For ScalaTest tests, there exists a special equality operator "===" that
   * can be used inside "assert". If the assertion fails, the two values will
   * be printed in the error message. Otherwise, when using "==", the test
   * error message will only say "assertion failed", without showing the values.
   *
   * Try it out! Change the values so that the assertion fails, and look at the
   * error message.
   */
  test("adding ints") {
    assert(1 + 2 === 3)
  }


  import FunSets._

  test("contains is implemented") {
    assert(contains(x => true, 100))
  }

  /**
   * When writing tests, one would often like to re-use certain values for multiple
   * tests. For instance, we would like to create an Int-set and have multiple test
   * about it.
   *
   * Instead of copy-pasting the code for creating the set into every test, we can
   * store it in the test class using a val:
   *
   * val s1 = singletonSet(1)
   *
   * However, what happens if the method "singletonSet" has a bug and crashes? Then
   * the test methods are not even executed, because creating an instance of the
   * test class fails!
   *
   * Therefore, we put the shared values into a separate trait (traits are like
   * abstract classes), and create an instance inside each test method.
   *
   */

  trait TestSets {
    val s0 = singletonSet(0)
    val s1 = singletonSet(1)
    val s2 = singletonSet(2)
    val s3 = singletonSet(3)
    val sMinus4 = singletonSet(-4)
    val sMinus5 = singletonSet(-5)
    val s345 = singletonSet(345)
    val sMinus54366 = singletonSet(-54366)
    val s2565 = singletonSet(2565)
  }

  /**
   * This test is currently disabled (by using "ignore") because the method
   * "singletonSet" is not yet implemented and the test would fail.
   *
   * Once you finish your implementation of "singletonSet", exchange the
   * function "ignore" by "test".
   */
  test("singletonSet(i) contains i") {

    /**
     * We create a new instance of the "TestSets" trait, this gives us access
     * to the values "s1" to "s3". 
     */
    new TestSets {
      /**
       * The string argument of "assert" is a message that is printed in case
       * the test fails. This helps identifying which assertion failed.
       */
      assert(contains(s1, 1), "s1 contains 1")
      assert(contains(s0, 0), "s0 contains 0")
      assert(contains(s2, 2), "s2 contains 2")
      assert(contains(s3, 3), "s3 contains 3")
      assert(contains(sMinus4, -4), "sMinus4 contains -4")
      assert(contains(sMinus5, -5), "sMinus5 contains -5")
      assert(contains(s345, 345), "s345 contains 345")
      assert(contains(sMinus54366, -54366), "sMinus54366 contains -54366")
      assert(contains(s2565, 2565), "s2565 contains 2565")
    }
  }

  test("union contains all elements") {
    new TestSets {
      val s = union(s1, s2)
      assert(contains(s, 1))
      assert(contains(s, 2))

      assert(contains(union(s, s3), 3))
      assert(contains(union(s, s2), 1))
      assert(contains(union(s, s2565), 2565))

      assert(!contains(s, 3))
      assert(!contains(union(s, s1), 2565))
    }
  }

  test("intersect contains all elements") {
    new TestSets {
      val s = intersect(s1, s2)
      assert(!contains(s, 1), "s1 intersect s2 !contains 1")
      assert(!contains(s, 2), "s1 intersect s2 !contains 2")

      val unionS1S2: Set = union(s1, s2)
      val unionS1S2Sminus5: Set = union(unionS1S2, sMinus5)

      assert(contains(intersect(unionS1S2, s1), 1), "s1s2 intersect s1 contains 1")
      assert(!contains(intersect(unionS1S2, s1), 2), "s1s2 intersect s1 !contains 2")
      assert(contains(intersect(unionS1S2, s2), 2), "s1s2 intersect s2 contains 2")
      assert(!contains(intersect(unionS1S2, s2), 1), "s1s2 intersect s2 !contains 1")

      assert(!contains(intersect(unionS1S2, s0), 0), "s1s2 intersect s0 !contains 0")
      assert(!contains(intersect(unionS1S2, sMinus54366), 1), "s1s2 intersect sMinus54366 !contains 1")
      assert(!contains(intersect(unionS1S2, sMinus54366), 2), "s1s2 intersect sMinus54366 !contains 2")
      assert(!contains(intersect(unionS1S2, s345), 1), "s1s2 intersect s345 !contains 1")
      assert(!contains(intersect(unionS1S2, s345), 2), "s1s2 intersect s345 !contains 2")

      assert(contains(intersect(unionS1S2Sminus5, s1), 1), "s1s2s-5 intersect s1 contains 1")
      assert(contains(intersect(unionS1S2Sminus5, s2), 2), "s1s2s-5 intersect s2 contains 2")
      assert(contains(intersect(unionS1S2Sminus5, sMinus5), -5), "s1s2s-5 intersect s-5 contains -5")

      assert(contains(intersect(unionS1S2Sminus5, unionS1S2), 1), "s1s2s-5 intersect s1s2 contains 1")
      assert(contains(intersect(unionS1S2Sminus5, unionS1S2), 2), "s1s2s-5 intersect s1s2 contains 2")
      assert(!contains(intersect(unionS1S2Sminus5, unionS1S2), -5), "s1s2s-5 intersect s1s2 !contains -5")
    }
  }
  
  test("diff contains all elements") {
    new TestSets {
      val unionS1S2: Set = union(s1, s2)
      val unionS1S2Sminus5: Set = union(unionS1S2, sMinus5)

      assert(!contains(diff(unionS1S2, s1), 1), "s1s2 diff s1 !contains 1")
      assert(contains(diff(unionS1S2, s1), 2), "s1s2 diff s1 contains 2")
      assert(!contains(diff(unionS1S2, s2), 2), "s1s2 diff s2 !contains 2")
      assert(contains(diff(unionS1S2, s2), 1), "s1s2 diff s2 contains 1")

      assert(!contains(diff(unionS1S2, s0), 0), "s1s2 diff s0 !contains 0")
      assert(contains(diff(unionS1S2, s0), 1), "s1s2 diff s0 contains 1")
      assert(contains(diff(unionS1S2, s0), 2), "s1s2 diff s0 contains 2")

      assert(!contains(diff(unionS1S2, sMinus54366), -54366), "s1s2 diff sMinus54366 !contains -54366")
      assert(contains(diff(unionS1S2, sMinus54366), 1), "s1s2 diff sMinus54366 contains 1")
      assert(contains(diff(unionS1S2, sMinus54366), 2), "s1s2 diff sMinus54366 contains 2")

      assert(!contains(diff(unionS1S2, s345), 345), "s1s2 diff s345 !contains 345")
      assert(contains(diff(unionS1S2, s345), 1), "s1s2 diff s345 contains 1")
      assert(contains(diff(unionS1S2, s345), 2), "s1s2 diff s345 contains 2")

      assert(!contains(diff(unionS1S2Sminus5, s1), 1), "s1s2s-5 diff s1 !contains 1")
      assert(contains(diff(unionS1S2Sminus5, s1), 2), "s1s2s-5 diff s1 contains 2")
      assert(contains(diff(unionS1S2Sminus5, s1), -5), "s1s2s-5 diff s1 contains -5")

      assert(!contains(diff(unionS1S2Sminus5, s2), 2), "s1s2s-5 diff s2 !contains 2")
      assert(contains(diff(unionS1S2Sminus5, s2), 1), "s1s2s-5 diff s2 contains 1")
      assert(contains(diff(unionS1S2Sminus5, s2), -5), "s1s2s-5 diff s2 contains -5")

      assert(!contains(diff(unionS1S2Sminus5, sMinus5), -5), "s1s2s-5 diff s-5 !contains -5")
      assert(contains(diff(unionS1S2Sminus5, sMinus5), 1), "s1s2s-5 diff s-5 contains 1")
      assert(contains(diff(unionS1S2Sminus5, sMinus5), 2), "s1s2s-5 diff s-5 contains 2")

      assert(!contains(diff(unionS1S2Sminus5, unionS1S2), 1), "s1s2s-5 diff s1s2 !contains 1")
      assert(!contains(diff(unionS1S2Sminus5, unionS1S2), 2), "s1s2s-5 diff s1s2 !contains 2")
      assert(contains(diff(unionS1S2Sminus5, unionS1S2), -5), "s1s2s-5 diff s1s2 contains -5")
    }
  }

  test("filter contains all elements") {
    new TestSets {
      assert(contains(filter(s345, x => true), 345), "s345 filter x is true contains 345")
      assert(contains(filter(s345, x => (x & 1) == 1), 345), "s345 filter x is odd contains 345")
      assert(!contains(filter(s345, x => (x & 1) == 0), 345), "s345 filter x is even !contains 345")
      assert(!contains(filter(s345, x => (x & 1) == 0), 0), "s345 filter x is even !contains 0")
      assert(!contains(filter(s345, x => (x & 1) == 0), -5), "s345 filter x is even !contains -5")

      assert(contains(filter(s0, x => true), 0), "s0 filter x is true contains 0")
      assert(!contains(filter(s0, x => (x & 1) == 1), 0), "s0 filter x is odd !contains 0")
      assert(!contains(filter(s0, x => (x & 1) == 0), 2565), "s0 filter x is even contains 2565")
      assert(!contains(filter(s0, x => (x & 1) == 0), 2565), "s0 filter x is even contains 2565")
    }
  }

//  test("forall false for outbound values") {
//    new TestSets {
//      assert(!forall(s2565, x => true), "forall is false for 2565")
//      assert(!forall(sMinus54366, x => true), "forall is false for -54366")
//    }
//  }

  test("forall true for inbound values and true lambda") {
    new TestSets {
      assert(forall(s0, x => true), "forall is true for s0")
      assert(forall(s1, x => true), "forall is true for s1")
      assert(forall(s2, x => true), "forall is true for s2")
      assert(forall(s3, x => true), "forall is true for s3")
      assert(forall(s345, x => true), "forall is true for s345")
      assert(forall(sMinus4, x => true), "forall is true for sMinus4")
      assert(forall(sMinus5, x => true), "forall is true for sMinus5")
    }
  }

  test("union forall is true for inbound values and true lambda") {
    new TestSets {
      assert(forall(union(s0, s1), x => true), "union forall is true for s0s1")
      assert(forall(union(s1, s2), x => true), "forall is true for s1s2")
      assert(forall(union(s3, s345), x => true), "forall is true for s3s345")
      assert(forall(union(sMinus4, sMinus5), x => true), "forall is true for sMinus4sMinus5")
    }
  }

  test("union forall is false for inbound values and false lambda") {
    new TestSets {
      assert(!forall(union(s0, s1), x => false), "union forall is false for s0s1")
      assert(!forall(union(s1, s2), x => false), "forall is false for s1s2")
      assert(!forall(union(s3, s345), x => false), "forall is false for s3s345")
      assert(!forall(union(sMinus4, sMinus5), x => false), "forall is false for sMinus4sMinus5")
    }
  }

  test("union forall is false for bound values and false lambda") {
    new TestSets {
      assert(!forall(union(singletonSet(-1000), singletonSet(1000)), x => false), "union forall is false for sMinus1000s1000")
    }
  }

  test("union forall is true for bound values and true lambda") {
    new TestSets {
      assert(forall(union(singletonSet(-1000), singletonSet(1000)), x => true), "union forall is true for sMinus1000s1000")
    }
  }

  test("union forall is true for bound values") {
    new TestSets {
      assert(forall(union(singletonSet(-1000), singletonSet(1000)), x => (x & 1) == 0), "union forall is true for sMinus1000s1000")
    }
  }

  test("union forall is false for bound values") {
    new TestSets {
      assert(!forall(union(singletonSet(-1000), singletonSet(1000)), x => (x & 1) == 1), "union forall is false for sMinus1000s1000")
    }
  }

  test("union exists is false for bound values and false lambda") {
    new TestSets {
      assert(!exists(union(singletonSet(-1000), singletonSet(1000)), x => false), "union exists is false for sMinus1000s1000")
    }
  }

  test("union exists is false for bound values and true lambda") {
    new TestSets {
      assert(exists(union(singletonSet(-1000), singletonSet(1000)), x => true), "union exists is true for sMinus1000s1000")
    }
  }

  test("union contains mapped values") {
    new TestSets {
      assert(contains(union(map(s2, x => x * x), map(s3, x => x * x)), 4), "union s2 2 s2*s2 and s3 2 s3 * s3 contains 4")
      assert(contains(union(map(s2, x => x * x), map(s3, x => x * x)), 9), "union s2 2 s2*s2 and s3 2 s3 * s3 contains 9")
      assert(contains(map(s0, x => x * x), 0), "s0 2 s0 * s0 contains 0")
      assert(contains(map(s345, x => x * x), 119025), "s345 2 s345 * s345 contains 119025")
      assert(!contains(map(s0, x => x * x), 1), "s0 2 s0 * s0 !contains 1")
      assert(!contains(map(s0, x => x * x), 6), "s0 2 s0 * s0 !contains 6")
      assert(!contains(map(s0, x => x * x), -6), "s0 2 s0 * s0 !contains -6")
    }
  }
}
