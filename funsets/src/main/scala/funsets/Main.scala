package funsets

object Main extends App {
  import FunSets._
  printSet(map(union(union(singletonSet(0), singletonSet(3)), singletonSet(6)), x=>x*x))
}
